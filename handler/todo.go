package handler

import (
	"net/http"
)

type FormData struct {
	Todo Todo
	Errors map[string]string
}

func (h *Handler) CreateTodo(rw http.ResponseWriter, r *http.Request) {
	vErrs := map[string]string{}
	todo := Todo{}
	h.loadCreatedTodo(rw, todo, vErrs)
}

func (h *Handler) StoreTodo(rw http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	task := r.FormValue("Task")
	todo := Todo{
		Task: task,
	}
	if task == "" {
		vErrs := map[string]string{
			"Task" : "This field is required",
		}
		h.loadCreatedTodo(rw, todo, vErrs)
		return
	}

	if (len(task) < 3) {
		vErr := map[string]string{
			"Task" : "This field must be greater than or equal 3",
		}
		h.loadCreatedTodo(rw, todo, vErr)
		return
	}

	const insertTodo = `INSERT INTO tasks(title, is_completed) VALUES ($1, $2);`
	res:= h.db.MustExec(insertTodo, task, false)

	if ok, err:= res.RowsAffected(); err != nil || ok == 0 {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}
	
	http.Redirect(rw, r, "/", http.StatusTemporaryRedirect)
}

func (h *Handler) CompleteTodo(rw http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/todos/complete/"):]
	if id == "" {
		http.Error(rw, "invalid URL", http.StatusInternalServerError)
		return
	}
	const completeTodo = `Update tasks SET is_completed = true WHERE id=$1`
	res:= h.db.MustExec(completeTodo, id)
	if ok, err:= res.RowsAffected(); err != nil || ok == 0 {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(rw, r, "/", http.StatusTemporaryRedirect)
}

func (h *Handler) EditTodo(rw http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/todos/edit/"):]
	if id == "" {
		http.Error(rw, "invalid URL", http.StatusInternalServerError)
		return
	}
	const getTodo = `SELECT * FROM tasks WHERE id=$1`
	var todo Todo
	h.db.Get(&todo, getTodo, id)

	if todo.ID == 0 {
		http.Error(rw, "invalid URL", http.StatusInternalServerError)
		return
	}
	
	h.loadEditTodo(rw, todo, map[string]string{})
}

func (h *Handler) UpdateTodo(rw http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/todos/update/"):]
	if id == "" {
		http.Error(rw, "invalid URL", http.StatusInternalServerError)
		return
	}

	const getTodo = `SELECT * FROM tasks WHERE id=$1`
	var todo Todo
	h.db.Get(&todo, getTodo, id)

	if todo.ID == 0 {
		http.Error(rw, "invalid URL", http.StatusInternalServerError)
		return
	}

	if err := r.ParseForm(); err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	task := r.FormValue("Task")
	todo.Task = task
	if task == "" {
		vErrs := map[string]string{
			"Task" : "This field is required",
		}
		h.loadEditTodo(rw, todo, vErrs)
		return
	}

	if (len(task) < 3) {
		vErrs := map[string]string{
			"Task" : "This field must be greater than 3",
		}
		h.loadEditTodo(rw, todo, vErrs)
		return
	}

	const updateTodo = `Update tasks SET title = $2 WHERE id=$1`
	res:= h.db.MustExec(updateTodo, id, task)
	if ok, err:= res.RowsAffected(); err != nil || ok == 0 {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(rw, r, "/", http.StatusTemporaryRedirect)
}

func (h *Handler) DeleteTodo(rw http.ResponseWriter, r *http.Request) {
	id := r.URL.Path[len("/todos/delete/"):]
	if id == "" {
		http.Error(rw, "invalid URL", http.StatusInternalServerError)
		return
	}

	const getTodo = `SELECT * FROM tasks WHERE id=$1`
	var todo Todo
	h.db.Get(&todo, getTodo, id)

	if todo.ID == 0 {
		http.Error(rw, "invalid URL", http.StatusInternalServerError)
		return
	}

	const deleteTodo = `DELETE FROM tasks WHERE id=$1`
	res:= h.db.MustExec(deleteTodo, id)
	if ok, err:= res.RowsAffected(); err != nil || ok == 0 {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(rw, r, "/", http.StatusTemporaryRedirect)
}

func (h *Handler) loadCreatedTodo(rw http.ResponseWriter,todo Todo, errs map[string]string) {
	form := FormData{
			Todo : todo,
			Errors : errs,
		}
		if err:= h.templates.ExecuteTemplate(rw, "create-todo.html", form); err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
		}
}
func (h *Handler) loadEditTodo(rw http.ResponseWriter,todo Todo, errs map[string]string) {
	form := FormData{
			Todo : todo,
			Errors : errs,
		}
		if err:= h.templates.ExecuteTemplate(rw, "edit-todo.html", form); err != nil {
		http.Error(rw, err.Error(), http.StatusInternalServerError)
		return
		}
}